const users = require('../users');

const getUser = (login) => {
    if (!login) {
        return false;
    }

    return users.find((user) => user.login == login);
}

module.exports = {
    getUser
}
