const getRandomInt = (min, max) => {
    // range: [min; max]
    max++;
    return (Math.floor(Math.random() * (max - min)) + min);
}

const getRandomIntProxy = new Proxy(getRandomInt, {
    apply(target, thisArg, argumentsList) {
        argumentsList.forEach(number => {
            if (!Number.isInteger(number)) {
                throw new TypeError(`Entered number ${number} is not integer`);
            }
        });

        return target.apply(thisArg, argumentsList);
    }
});

module.exports = {
    getRandomInt: getRandomIntProxy
};