class Commentator {
    constructor(options) {
        this.name;
        this.participants;
        this.io;

        this.mainMessageInterval;
        this.randomMessageInterval;
        this.timerMainMessage;
        this.timerRandomMessage;
        this.messagePriority;

        this.symbolsRemaining;
    }

    init(name, io) {
        this.name = name;
        this.io = io;
        this.randomMessageInterval = 4000;  // ms
        this.mainMessageInterval = 10000;   // ms
        this.symbolsRemaining = 30;
        this.messagePriority = true;   // true: main message, false: random message
    }

    welcome(logins) {
        let comment = `Привіт, сьогодні прекрасний день не тільки тому, що ми живі, а й тому, що сьогодні на нас чекають чудові перегони.<br>
        А описувати це шалене дійство буду я - ${this.name}.<br>
        А тепер, хочу Вам представити учасників гонки:<br>`;
        logins.forEach(login => {
            comment += login + '<br>';
        });
        comment += 'Ну що ж, розпочинаємо!'

        return comment;
    }

    informStateRace() {
        this.timerMainMessage = setInterval(() => {
            this.setMessagePriority();

            const participants = ([], this.participants);

            const timeToFinish = this.timeToFinish(participants[0]);
            const room = participants[0].room;
            let comment = '';

            participants.sort((a, b) => b.lengthEnteredSymbols - a.lengthEnteredSymbols);

            participants.forEach((participant, index) => {
                comment += `${index + 1}. ${participant.login} - ${participant.lengthEnteredSymbols} км`;
                if (index) {
                    comment += `, відстань до першого місця: ${participant.lengthEnteredSymbols - participants[index - 1].lengthEnteredSymbols}`;
                }
                comment += `<br>`;
            });
            comment += `До завершення залишилось: ${timeToFinish} секунд.`;

            this.io.to(room).emit('commentator', { comment });
        }, this.mainMessageInterval);
    }

    timeToFinish(participant) {
        return (participant.raceTime - Date.parse(new Date())) / 1000;
    }

    setMessagePriority() {
        this.messagePriority = true;

        setTimeout(() => {
            this.messagePriority = !this.messagePriority;
        }, this.randomMessageInterval);
    }

    stopTimer() {
        clearInterval(this.timerMainMessage);
        clearInterval(this.timerRandomMessage);
    }

    finishLine(socket) {
        this.setMessagePriority();

        if (socket.lengthEnteredSymbols === socket.text.length - this.symbolsRemaining) {
            const comment = `${socket.login} наближається до фінішної прямої.`;

            this.io.to(socket.room).emit('commentator', { comment });
        }
    }

    finished(socket) {
        if (socket.lengthEnteredSymbols === socket.text.length) {
            const comment = `${socket.login} - фінішував!`;
            this.io.to(socket.room).emit('commentator', { comment });
        }
    }

    endGame() {
        this.stopTimer();
        const participants = ([], this.participants);
        participants.sort((a, b) => b.lengthEnteredSymbols - a.lengthEnteredSymbols);

        let comment = 'Трійка переможців визначена!<br>';

        participants.forEach((participant, index) => {
            comment += `${index + 1}. ${participant.login}, час заїзду - ${participant.timePassed}<br>`;
        });

        comment += 'Це була грандіозна гонка. А на цьому у нас все, усім дякую за увагу та до наступної гонки! - ігого...';
        this.io.to(participants[0].room).emit('commentator', { comment });
    }

    arbitraryMessage() {
        const participants = ([], this.participants);
        this.timerRandomMessage = setInterval(() => {
            if (this.messagePriority) {
                return;
            }

            const comment = 'random message';
            this.io.to(participants[0].room).emit('commentator', { comment });

        }, this.randomMessageInterval);
    }
}

const createCommentator = () => {
    return new Commentator();
}

module.exports = createCommentator;
