// можливо використати pattern facade
const startTime = (offsetMinutes = 0) => {
    const duration = (.5 + offsetMinutes) * 60 * 1000; // ms
    const timestamp = new Date(Date.parse(new Date()) + duration);

    return timestamp;
}

const raceTime = (offsetMinutes = 0) => {
    const duration = (3 + offsetMinutes) * 60 * 1000; // ms
    const timestamp = new Date(Date.parse(new Date()) + duration);

    return timestamp;
}

module.exports = {
    startTime,
    raceTime
};