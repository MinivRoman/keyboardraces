const path = require('path');
const bodyParser = require('body-parser');

const express = require('express');
const app = express();
const server = require('http').Server(app);

const io = require('socket.io')(server);

const { getUser } = require('./repositories/user.repository');
const { sign, authenticate, verify } = require('./middlewares/authenticate.middleware');
const { startTime, raceTime } = require('./services/race.service');
const { getRandomText } = require('./repositories/text.repository');
const createCommentator = require('./services/commentator.service');

const port = 3000;

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.get('/login', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'login.html'));
});

app.post('/login', (req, res, next) => {
    const { login, password } = req.body;

    if (login && password) {
        const user = getUser(login);
        if (!user) {
            return res.status(401).json({ err: "No such user found" });
        }

        if (user.password == password) {
            const payload = { login: user.login };
            const jwt = sign(payload);

            res.json({ jwt });
        } else {
            res.status(401).json({ err: "Password is incorrect" });
        }
    } else {
        res.status(401).json({ err: "Not found data" });
    }
});

app.post('/textRace', authenticate, (req, res, next) => {
    res.json(getRandomText());
});

app.post('/', authenticate, (req, res, next) => {
    const { login } = req.user;
    const user = getUser(login);
    delete user.password;
    res.json(user);
});

app.get('/', (req, res, next) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

server.listen(port, (err) => {
    if (err) {
        return console.error(`Error ${err}`);
    }
});

const race = {
    startTime: startTime(),
    raceTime: raceTime(),
    textRace: getRandomText()
};

let room = 1;
const socketList = new Map();
const commentator = createCommentator();

io.on('connection', (socket) => {
    socket.on('connected', (payload) => {
        if (io.sockets.adapter.rooms[room] &&
            io.sockets.adapter.rooms[room].length >= 4) {
            room++;
            race.startTime = startTime();
            race.raceTime = raceTime();
            race.textRace = getRandomText();
        }

        const { jwt } = payload;
        payload.login = verify(jwt).login;
        delete payload.jwt;

        payload.room = room;
        payload.startTime = race.startTime;

        socket.join(payload.room);
        socketList.set(socket.id, payload);

        socket.emit('waitingRace', payload);
    });

    socket.on('startRace', () => {
        const payload = {};

        payload.textRace = race.textRace;
        payload.raceTime = raceTime();

        const currSocket = socketList.get(socket.id);
        currSocket.text = payload.textRace;
        currSocket.raceTime = payload.raceTime;
        currSocket.timePassed = (currSocket.raceTime - Date.parse(new Date())) / 1000;

        const participants = [];

        socketList.forEach((roomSocket) => {
            if (roomSocket.room == socketList.get(socket.id).room) {
                participants.push(roomSocket);
            }
        });

        payload.logins = participants.map((participant) => {
            return participant.login;
        });

        socket.emit('startRace', payload);

        commentator.init('Anon Makaron', io);
        const comment = commentator.welcome(payload.logins);
        commentator.participants = participants;
        commentator.informStateRace();
        commentator.arbitraryMessage()
        socket.emit('commentator', { comment });
    });

    socket.on('progress', (payload) => {
        const currSocket = socketList.get(socket.id);
        currSocket.lengthEnteredSymbols = payload.lengthEnteredSymbols;
        if (payload.timePassed) {
            currSocket.timePassed = payload.timePassed;
        }
        
        payload.login = socketList.get(socket.id).login;

        io.to(currSocket.room).emit('progress', payload);
        commentator.finishLine(socketList.get(socket.id));
        commentator.finished(socketList.get(socket.id));
    });

    socket.on('raceResult', (payload = {}) => {
        payload.socketList = [];

        socketList.forEach((roomSocket) => {
            if (roomSocket.room == socketList.get(socket.id).room) {
                payload.socketList.push(roomSocket);
            }
        });

        socket.emit('raceResult', payload);
        commentator.endGame();
    });

    socket.on('disconnect', () => {
        socketList.delete(socket.id);
        socket.leave(room);
    });
}); 
