class Timer {
    constructor() {
        this.timerId;
        this.startTimestamp;
        this.endTime;
        this.timeRemaining;
        this.timeMsg;
        this.callback;
    }

    static renderElement;

    init = (timestamp, renderElement, timeMsg, callback) => {
        this.startTimestamp = Date.parse(new Date());
        this.endTime = Date.parse(timestamp);
        Timer.renderElement = renderElement;
        this.timeMsg = timeMsg;
        this.callback = callback;
    }

    startTimer = () => {
        this.tick();
        this.timerId = setInterval(() => {
            this.tick();
        }, 1000);
    }

    tick = () => {
        const timeDifference = this.endTime - Date.parse(new Date());

        const seconds = this.setTimeFormat(Math.floor((timeDifference / 1000) % 60));
        const minutes = this.setTimeFormat(Math.floor((timeDifference / 1000 / 60) % 60));
        const hours = this.setTimeFormat(Math.floor((timeDifference / 1000 / 60 / 60) % 24));

        const currentTime = `${hours}:${minutes}:${seconds}`;

        Timer.renderElement.textContent = `${this.timeMsg}: ${currentTime}`;

        this.timeRemaining = currentTime;

        if (timeDifference <= 0) {
            this.stopTimer();
        }
    }

    setTimeFormat = (unitTime) => {
        return `0${unitTime}`.slice(-2);
    }

    stopTimer = () => {
        clearInterval(this.timerId);
        this.callback();
    }

    timePassed = () => {
        const timeRemaining = this.endTime - Date.parse(new Date());
        const fullTime = this.endTime - this.startTimestamp;

        const passedSeconds = Math.floor((fullTime - timeRemaining) / 1000);

        return passedSeconds;
    }
};
