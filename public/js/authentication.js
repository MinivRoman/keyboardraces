const jwt = sessionStorage.getItem("jwt");

if (!jwt) {
    location.replace('login');
}
