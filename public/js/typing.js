class Typing {
    constructor() {
        this.activeSymbol;
        this.textElement;
        this.text;
    }

    init = (textElement) => {
        this.activeSymbol = 0;

        this.textElement = textElement;
        this.text = this.textElement.textContent.split('');

        this.highlightActiveSymbol();
        this.updateHighlight();
    }

    highlightActiveSymbol = () => {
        this.text[this.activeSymbol] = `<span class='activeSymbol'>${this.text[this.activeSymbol]}</span>`;
    }

    updateHighlight = () => {
        this.textElement.innerHTML = this.text.join('');
    }

    textHighlight = () => {
        this.text[this.activeSymbol] = this.text[this.activeSymbol].replace(/activeSymbol/i, 'typedText');

        this.activeSymbol++;
        if (this.activeSymbol < this.text.length) {
            this.highlightActiveSymbol();
        }

        this.updateHighlight();
    }

    typing = (e) => {
        if (e.key === this.textElement.textContent[this.activeSymbol]) {
            this.textHighlight();
        }

        return this.activeSymbol;
    }
}
