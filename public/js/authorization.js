const formAuth = document.forms.authorization;

formAuth.addEventListener('submit', (e) => {
    e.preventDefault();

    const userData = {
        login: formAuth.login.value,
        password: formAuth.password.value
    };

    fetch('login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
        .then((res) => res.json())
        .then((data) => {
            if (data.err) {
                return;
            }

            sessionStorage.setItem('jwt', data.jwt);
            location.replace('/');
        }).catch((err) => {
            console.error(err);
        });
});
