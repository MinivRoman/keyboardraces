const rootElement = document.getElementById('root');

const waitingRaceElement = document.getElementById('waitingRace');
const loginElement = document.getElementById('login');
const roomElement = document.getElementById('room');
const timerElement = document.getElementById('timer');

const raceElement = document.getElementById('race');
const raceTimerElement = document.getElementById('raceTimer');
const playersElement = document.getElementById('players');
const textRaceElement = document.getElementById('textRace');

const resultElement = document.getElementById('result');
const nextRaceTimerElement = document.getElementById('nextRaceTimer');
const winnersElement = document.getElementById('winners');

const commentatorElement = document.getElementById('commentator');
const commentatorDescriptionElement = document.getElementById('commentator-description');

const socket = io.connect('http://localhost:3000');
const timer = new Timer;
const typing = new Typing;

socket.emit('connected', { jwt });

socket.on('waitingRace', (payload) => {
    const { login, room, startTime } = payload;

    waitingRace.style.display = 'flex';

    timer.init(startTime, timerElement, 'Time to start', startRace);
    timer.startTimer();

    loginElement.textContent = `Player: ${login}`;
    roomElement.textContent = `Room: ${room}`;
});


const startRace = () => {
    socket.emit('startRace');
    
    socket.on('startRace', (payload) => {
        const { logins, textRace, raceTime } = payload;

        waitingRaceElement.style.display = 'none';
        raceElement.style.display = 'flex';
        commentatorElement.style.display = 'flex';

        const players = document.createDocumentFragment();

        logins.forEach((login) => {
            const li = document.createElement('li');
            li.setAttribute('data-login', login);

            const span = document.createElement('span');
            span.textContent = login;
            li.appendChild(span);

            const progress = document.createElement('progress');
            progress.setAttribute('value', 0);
            progress.setAttribute('max', 1);
            li.appendChild(progress);

            players.appendChild(li);
        });

        playersElement.appendChild(players);

        textRaceElement.textContent = textRace;
        typing.init(textRaceElement);

        timer.init(raceTime, raceTimerElement, 'Time to end', endRace);
        timer.startTimer();
    });

    socket.on('progress', (payload) => {

        const { login, lengthEnteredSymbols } = payload;
        const progressElement = playersElement.querySelector(`[data-login=${login}]`)
            .querySelector('progress');

        const progressValue = lengthEnteredSymbols / textRaceElement.textContent.length;
        progressElement.value = progressValue;

        checkAllTypedText(progressElement);
    });

    socket.on('commentator', (payload) => {
        const { comment } = payload;
        commentatorDescriptionElement.innerHTML = comment;
    });
}

const checkAllTypedText = (currProgress) => {
    if (currProgress.value >= 1) {
        const progressElements = players.querySelectorAll('progress');

        let allTypedText = true;

        for (let i = 0; i < progressElements.length; i++) {
            if (progressElements[i].value < 1) {
                allTypedText = false;
            }
        }

        if (allTypedText) {
            timer.stopTimer();
        }
    }
}

const endRace = () => {
    socket.emit('raceResult');

    socket.on('raceResult', (payload) => {
        const { socketList } = payload;

        socketList.forEach((socket) => {
            socket.relation = socket.lengthEnteredSymbols / socket.timePassed;
        });

        socketList.sort((a, b) => b.relation - a.relation);

        raceElement.style.display = 'none';
        resultElement.style.display = 'flex';

        const winners = document.createDocumentFragment();

        socketList.forEach((socket) => {
            const li = document.createElement('li');
            li.textContent = `${socket.login}, ${socket.relation.toFixed(2)} (symbols/time)`;
            winners.appendChild(li);
        });

        winnersElement.appendChild(winners);

        const startTime = new Date(Date.parse(new Date()) + 20000)
        timer.init(startTime, nextRaceTimerElement, 'Time to next race', () => {
            // new race
        });
        timer.startTimer();

    });
}

document.addEventListener('keydown', (e) => {
    if (!textRaceElement.textContent) {
        return;
    }

    const payload = {};
    payload.lengthEnteredSymbols = typing.typing(e);

    if (payload.lengthEnteredSymbols == typing.text.length) {
        payload.timePassed = timer.timePassed();
    }

    socket.emit('progress', payload);
});

const reset = () => {
    loginElement.innerHTML = '';
    roomElement.innerHTML = '';
    timerElement.innerHTML = '';

    raceTimerElement.innerHTML = '';
    playersElement.innerHTML = '';
    textRaceElement.innerHTML = '';
    raceElement.style.display = 'none';

    nextRaceTimerElement.innerHTML = '';
    winnersElement.innerHTML = '';
    textRaceElement.innerHTML = '';
    resultElement.style.display = 'none';
}